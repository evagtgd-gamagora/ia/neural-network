// char-frequency.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <filesystem>
#include <fstream>
#include <stdio.h>
#include <vector>
#include "common.h"

using namespace std;
namespace fs = std::filesystem;

static vector<double> paramLetterFrequency(const string path);
static double* letterFrequency(const string path, double* freq);
static void writeNetworkLearnData(vector<LearnData> data);
static void writeNetworkTestData(vector<TestData> data);

int main()
{
	int n_source_files = 0;
	int n_test_files = 0;

	//Build data for network learning step
	cout << endl << "## Data to learn : " << endl;
	vector<LearnData> learnData;
	
	for (auto & language_entry : Language::map)
	{
		string floder_path = f_sources + language_entry.second;

		cout << "# Language : " << language_entry.second << endl;
		for (auto & p : fs::directory_iterator(floder_path))
		{
			LearnData ld;
			++n_source_files;
			cout << "Source " << n_source_files << " : ";

			vector<double> freqData = paramLetterFrequency(p.path().string());
			for (auto & f : freqData)
			{
				cout << f << "; ";
				ld.inputs.push_back(f);
			}

			for (auto & l : Language::map)
			{
				double result = (l.first == language_entry.first) ? 1 : -1;
				ld.outputs.push_back(result);
			}

			learnData.push_back(ld);
				
			cout << endl;
		}
	}

	writeNetworkLearnData(learnData);


	//Build data for network test step
	cout << endl << "## Data to test : " << endl;
	vector<TestData> testData;

	for (auto & p : fs::directory_iterator(f_tests))
	{
		TestData td;
		++n_test_files;
		cout << "Test " << n_test_files << " : ";

		td.id = p.path().string();

		vector<double> freqData = paramLetterFrequency(p.path().string());
		for (auto & f : freqData)
		{
			cout << f << "; ";
			td.inputs.push_back(f);
		}

		testData.push_back(td);
			
		cout << endl;
	}

	writeNetworkTestData(testData);
}

static vector<double> paramLetterFrequency(const string path)
{
	vector<double> data;

	double freq[nLetter];

	letterFrequency(path, freq);

	for (auto & letter_entry : Letter::map)
	{
		char c = letter_entry.second;
		data.push_back(freq[c - 'a']);
	}

	return data;
}

static double* letterFrequency(const string path, double* freq)
{
	for (int i = 0; i < nLetter; ++i)
		freq[i] = 0;

	int sumLetter = 0;

	//Open file and count characters
	ifstream ifs;
	char c;

	ifs.open(path, fstream::in);

	if (ifs.is_open())
	{
		while (ifs.get(c))
		{
			if (c >= 'A' && c <= 'Z')
			{
				freq[c - 'A'] += 1.0f;
				++sumLetter;
			}
			else if (c >= 'a' && c <= 'z')
			{
				freq[c - 'a'] += 1.0f;
				++sumLetter;
			}
			else if (c=='é' || c=='è')
			{
				freq['e' - 'a'] += 1.0f;
				++sumLetter;
			}
			else if (c == 'ä' || c == 'à')
			{
				freq['a' - 'a'] += 1.0f;
				++sumLetter;
			}
			else if (c == 'ù' || c == 'ü')
			{
				freq['u' - 'a'] += 1.0f;
				++sumLetter;
			}
			//if (c != ' ' && c != '\n' && c != '!' && c != '?' && c != '.' && (c < '0' || c > '9'))
			//	++sumLetter;
		}

		//Frequency
		if (sumLetter > 0)
		{
			for (int i = 0; i < nLetter; ++i)
				freq[i] /= sumLetter;
		}

		ifs.close();
	}
	
	return freq;
}

static void writeNetworkLearnData(vector<LearnData> data)
{
	fs::remove(f_data + n_sources);

	ofstream ofs(f_data + n_sources);

	if (ofs.is_open())
	{
		ofs << data.size() << " " << Letter::N << " " << Language::N << endl;

		for (auto & d : data)
		{
			for (auto & v : d.inputs)
				ofs << to_string(v) + " ";
			
			ofs << endl;

			for (auto & v : d.outputs)
				ofs << to_string(v) + " ";

			ofs << endl;
		}
		ofs.close();
	}
}

static void writeNetworkTestData(vector<TestData> data)
{
	fs::remove(f_data + n_tests);

	ofstream ofs(f_data + n_tests);

	if (ofs.is_open())
	{
		ofs << data.size() << " " << Letter::N << " " << Language::N << endl;

		for (auto & d : data)
		{
			ofs << d.id << endl;

			for (auto & v : d.inputs)
				ofs << to_string(v) + " ";

			ofs << endl;
		}

		ofs.close();
	}
}