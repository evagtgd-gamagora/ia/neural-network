De la grande année selon les opinions de divers auteurs; de plusieurs 
autres révolutions; des olympiades; des lustres, et des jeux Capitolins; en 
quelle année ce livre a été composé. 
 Mais c'en est assez à propos du siècle; maintenant je parlerai des grandes 
années, dont la longueur est si diverse, soit dans les pratiques des peuples, 
soit dans les traditions des auteurs, que les uns font consister la grande année 
dans la réunion de deux années solaires, les autres dans le concours de 
plusieurs milliers d'années. Je vais chercher à expliquer ces différences. 
Plusieurs anciennes cités de la Grèce, ayant remarqué que, pendant l'année 
qu'emploie le soleil à accomplir sa révolution, il y avait quelquefois treize 
levers de la lune, et que cela arrivait une fois tous les deux ans, ont pensé 
qu'à l'année solaire répondaient douze mois lunaires et demi; elles établirent 
donc leurs années civiles de manière à ce que, par une intercalation, les unes 
se composassent de douze mois et les autres de treize, appelant année solaire 
chacune d'elles prise isolément, et grande année la réunion de deux année 
solaires. Et cet espace de temps était appelé triétéride, parce que 
l'intercalation d'un mois avait lieu à chaque troisième année, bien que cette 
révolution n'embrassât que deux ans, et ne fût en réalité qu'une diétéride; 
c'est ce qui fait que les mystères célébrés une fois tous les deux ans, en 
l'honneur de Bacchus, sont nommés triétériques par les poëtes. Ayant plus tard 
reconnu leur erreur, les anciens ont doublé cet espace de temps, et établi la 
tétraétéride, qui, parce qu'elle revenait à chaque cinquième année, fut nommée 
pentaétéride. Cette formation de la grande année par la réunion de quatre années 
solaires parut plus commode; car, l'année solaire se composant de trois cent 
soixante-cinq jours et un quart environ, cette fraction permettait d'ajouter, 
tous les quatre ans, un jour plein à la quatrième année. Voilà pourquoi, au 
retour de chaque quatrième année, on célèbre des jeux dans l'Élide en l'honneur 
de Jupiter Olympien, et à Rome, en l'honneur de Jupiter Capitolin. Ce temps, 
aussi, qui semblait ne répondre qu'au cours du soleil, et non à celui de la 
lune, fut encore doublé, et l'on établit l'octaétéride, qu'on appela 
ennéaétéride, parce qu'elle reparaissait à chaque neuvième année; et cet espace 
de temps fut considéré, par presque toute la Grèce, comme la véritable grande 
année, parce qu'elle résulte d'un nombre d'années naturelles sans fraction, 
comme cela doit avoir lieu pour toute grande année. Celle-ci, en effet, se 
composait de quatre-vingt-dix-neuf jours pleins et huit années naturelles aussi 
sans fraction. L'institution de cette octaétéride est généralement attribuée à 
Eudoxe de Cnide; mais c'est à Cléostrate de Ténédos qu'appartient, dit-on, 
l'honneur de l'avoir inventée. Ainsi ont fait Harpalus, Nautélès, Mnésistrate, 
et d'autres encore, parmi lesquels Dosithée, dont le travail a pour titre: 
l'Octaétéride d'Eudoxe. De là vient qu'en Grèce on célèbre, avec grande 
cérémonie, plusieurs fêtes religieuses après cette période de temps. A Delphes, 
aussi, les jeux qu'on appelle Pythiques étaicelle célébrés autrefois tous les 
huit ans. La grande année qui se rapproche le plus de celle-ci est la 
dodécaétéride, formée de douze années solaires. On l'appelle année chaldaïque: 
les astrologues de la Chaldée l'ont réglée non sur le cours du soleil et de la 
lune, mais d'après d'autres observations; parce qu'il ne faut pas moins, 
disent-ils, que cette révolution de temps pour embrasser les diverses saisons, 
les époques d'abondance, de stérilité, et de maladies. Il y a encore plusieurs 
autres grandes années, tells que l'année métonique, composée de dix-neuf années 
solaires, par Méton d'Athènes: aussi l'appelle-t-on ennéadécaétéride; on y 
intercale sept mois, et l'on y trouve six mil neuf cent quarante jours. On 
distingue aussi l'année du pythagoricien Philolaüs, formée de cinquante-neuf 
années solaires, avec l'intercalation de vingt et un mois; puis celle de 
Démocrite, formée de quatre-vingt-deux ans, à la faveur de vingt-huit mois 
intercalaires; puis encore celle d'Hipparque, composée de trois cent quatre ans, 
avec l'intercalation de cent douze mois. Cette différence de longueur des 
grandes années tient à ce que les astrologues ne sont pas d'accord sur le point 
de savoir ni la fraction qu'il faut ajouter aux trois cent soixante-cinq jours 
de l'année solaire, ni ce qu'il faut ôter aux trente jours du mois lunaire. Les 
Égyptiens, dans la formation de leur grande année, n'ont aucun égard à la lune 
appelée par les Grecs kuniko`V, par les Latins canicularis, par la raison 
qu'elle commence avec le lever de la canicule, le premier jour du mois que les 
Égyptiens appellent thoth. En effet, leur année civile n'a que trois cent 
soixante-cinq jours, sans aucune intercalation. Aussi l'espace de quatre ans 
est-il, chez eux, plus court d'un jour environ que l'espace de quatre années 
naturelles; ce qui fait que la correspondance ne se rétablit qu'à la quatorze 
cent soixante et unième année. Cette année est aussi appelée par quelques-uns 
héliaque, et par d'autres l'année de Dieu. Il y a encore l'année nommée par 
Aristote suprême, plutôt que grande, et que forment les révolutions du soleil, 
de la lune et des cinq étoiles errantes, lorsque tous ces astres sont revenus au 
point d'où ils étaient partis. Cette année a un grand hiver, appelé par les 
Grecs kataklusmo`V, c'est-à-dire déluge; puis, un grand été, nommé ejkpuvrwsiV, 
ou incendie du monde. Le monde, en effet, semble être tour à tour inondé ou 
embrasé à chacune de ces époques. Cette année, d'après l'opinion d'Aristarque, 
se compose de deux mille quatre cent quatre-vingt-quatre années solaires. Arétès 
de Dyrrachium la fait de cinq mille cinq cent cinquante-deux années; 