rius et de S. P. Verginius. Les seconds jeux ont eu lieu, selon Valerius 
d'Antium, l'an 305 de la fondation de Rome, et, d'après les commentaires des 
quindécemvirs, l'an 408, sous le deuxième consulat de M. Valerius Corvinus et 
sous celui de C. Pétilius. Les troisièmes ont eu lieu, d'après l'Antiate et 
Tite-Live, sous le consulat de P. Claudius Pulcher et de C. Junius Pullus, ou, 
comme on le voit dans les livres des quindécemvirs, en l'an 518, sous le 
consulat de P. Cornelius Lentulus et de C. Licinius Varus. Quant à l'année où 
furent célébrés les quatrièmes jeux, il y a trois opinions. L'Antiate, Varron et 
Tite-Live nous apprennent qu'ils eurent lieu en l'an 605 de la fondation de 
Rome, sous le consulat de L. Martius Censorinus et de M. Manilius; mais Pison 
l'ex-censeur, Cn. Gellius et Cassius Hemina, qui vivait à cette époque, 
affirment qu'ils ont eu lieu trois années plus tard, l'an 608, sous le consulat 
de Cn. Cornelius Lentulus et de L. Mummius Achaïcus; au contraire, d'après les 
commentaires des quindécemvirs, ils sont indiqués sous l'année 628, consulat de 
M. Émilius Lepidus et de L. Aurelius Orestès. Les cinquièmes jeux furent 
célébrés par César Auguste et par Agrippa, l'an 737, sous le consulat de C. 
Furnius et de C. Junius Silanus. Les sixièmes le furent par Tib. Claudius César, 
sous son quatrième consulat et sous le troisième de L. Vitellius, l'an de Rome 
800. Les septièmes le furent par Domitien, sous son quatorzième consulat et sous 
celui de L. Minucius Rufus, en l'an 841. Les huitièmes furent célébrés par les 
empereurs Septimius et M. Aurelius Antoninus, sous le consulat de Cilon et de 
Libon, l'an de Rome 957. D'où l'on peut voir que ce n'est ni tous les cent ans, 
ni tous les cent dix ans, que ces jeux devaient être célébrés. Et quand bien 
même on aurait observé dans le passé l'une ou l'autre de ces périodes de temps, 
cela ne suffirait point toutefois pour qu'on se crût en droit d'affirmer que 
toujours les jeux Séculaires distinguassent la fin d'un siècle; d'autant plus 
que, de la fondation de Rome jusqu'à l'expulsion des rois, il s'est écoulé deux 
cent quarante-quatre années, et qu'aucun auteur ne nous apprend que ces jeux 
aient eu lieu dans cet intervalle, qui, sans contredit, est plus long qu'un 
siècle naturel. Que si, trompé par la seule étymologie du mot, quelqu'un pensait 
que les siècles se distinguaient par les jeux Séculaires, qu'il sache que ces 
jeux peuvent avoir été nommés ainsi, parce que généralement chaque homme les 
voit célébrer une fois dans sa vie. C'est ainsi que, dans le langage habituel, 
on dit de beaucoup d'autres choses qui n'ont lieu que rarement, qu'on ne les 
voit qu'une fois dans un siècle. 