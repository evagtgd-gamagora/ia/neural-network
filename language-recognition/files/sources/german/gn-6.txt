Frau Meier geht heute in den Supermarkt. Ihr Mann ist nicht zu Hause und sie nimmt den Bus. An der Haltestelle trifft sie ihre Freundin Frau Schmidt. Frau Schmidt will auch in den Supermarkt.

Frau M�ller: �Das ist super! Dann fahren wir zusammen!�.
Frau Schmidt: �Ja, wir kaufen ein. Danach trinken wir Kaffee und essen Kuchen. Im Caf� neben dem Supermarkt gibt es sehr guten Kuchen.�
Frau Meier: �Gute Idee!�.

Frau Meier und Frau Schmidt gehen zusammen in den Supermarkt.

Frau Meier: �Ich brauche Tomaten. Mein Mann will Salat essen. Ich nehme zehn Tomaten.�
Frau Schmidt: �Tomaten sind gut. Ich kaufe f�nf Tomaten. Da ist auch Kopfsalat.�

Frau Meier nimmt keinen Kopfsalat. Sie nimmt aber zwei Gurken. Frau Meier kauft auch ein Kilo Zwiebeln. Frau Schmidt will Brot kaufen.

Frau Meier: �Das Brot ist nicht sch�n. Ich kaufe Brot in der B�ckerei. Aber die Schokolade hier ist gut. Sie kostet nur 50 Cent. Ich nehme drei Tafeln Schokolade.�
Frau Schmidt: �Da ist noch ein Sonderangebot. Mineralwasser und Orangensaft sind billig.�
Frau Meier: �Orangensaft habe ich zu Hause. Aber ich brauche f�nf Flaschen Mineralwasser.�

Frau Schmidt kauft nur drei Flaschen Mineralwasser. Frau Meier und Frau Schmidt gehen an die Kasse. Dann gehen sie ins Caf�. Frau Schmidt trinkt eine Tasse Tee. Frau Meier mag lieber Kaffee. Sie bestellen zwei St�cke Schokoladenkuchen. Sie fahren mit dem Bus nach Hause.