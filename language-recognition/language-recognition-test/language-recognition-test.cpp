// language-recognition-test.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <filesystem>
#include <fstream>
#include <iostream>
#include "fann.h"
#include "common.h"

using namespace std;
namespace fs = std::filesystem;

static vector<TestData> interpreteTestFile();
static void writeTestResults(vector<ResultData> data);

int main()
{

	const unsigned int num_input = Letter::N;
	const unsigned int num_output = Language::N;

	vector<TestData> testData = interpreteTestFile();
	vector<ResultData> resultData;

	const unsigned int num_tests = testData.size();


	string net = f_data + n_net;
	struct fann *ann = fann_create_from_file(net.c_str());

	fann_type * pInputs = NULL;
	fann_type * pOutputs = NULL;

	cout << "LANGUAGE DETECTION: " << endl;

	for (TestData td : testData)
	{
		cout << "Name : " << td.id << endl;
		cout << "Inputs : ";

		vector<fann_type> inputs;
		for (double d : td.inputs)
		{
			cout << d << " ";
			inputs.push_back((float)d);

		}

		cout << endl;

		pInputs = &inputs[0];
		pOutputs = fann_run(ann, pInputs);

		vector<double> outputs;
		cout << "Ouput : ";
		for (int j = 0; j < num_output; ++j)
		{
			cout << pOutputs[j] << " ";
			outputs.push_back(pOutputs[j]);
		}
		cout << endl << endl;

		ResultData rd;
		rd.testData = td;
		rd.outputs = outputs;
		resultData.push_back(rd);
	}

	writeTestResults(resultData);
	fann_destroy(ann);
	system("pause");

	return 0;
}

static vector<TestData> interpreteTestFile()
{
	//Open file and count characters
	string test_path = f_data + n_tests;
	ifstream ifs;
	ifs.open(test_path, fstream::in);

	const int maxLine = 256;
	char param[maxLine];
	ifs.getline(param, maxLine);

	int num_tests = 0;

	//get number of test
	for (int i = 0; i < maxLine - 1; ++i)
	{
		if (param[i] >= '0' && param[i] <= '9')
			num_tests += param[i] - '0';

		if (param[i + 1] == ' ' || param[i + 1] == '\n')
			break;
		else
			num_tests *= 10;
	}

	//get tests
	vector<TestData> testData;
	for (int i = 0; i < num_tests; ++i)
	{
		TestData td;
		char name[maxLine];
		char values[maxLine];

		ifs.getline(name, maxLine);
		ifs.getline(values, maxLine);

		string s_name = name;
		s_name = s_name.substr(0, s_name.find(" "));
		td.id = s_name;

		//get inputs
		string s_values = values;
		int pos = 0;
		for (int i = 0; i < Letter::N; ++i)
		{
			string s_v = s_values.substr(pos, s_values.find(" "));
			pos = s_values.find(" ") + 1;

			double v = stod(s_v);
			td.inputs.push_back(v);
		}
		
		testData.push_back(td);
	}

	return testData;
}

static void writeTestResults(vector<ResultData> data)
{
	fs::remove(f_data + n_results);

	ofstream ofs(f_data + n_results);

	if (ofs.is_open())
	{

		for (auto & d : data)
		{
			ofs << d.testData.id << endl;

			int i = 0;
			for (auto & v : d.outputs)
			{
				ofs << Language::map.at(static_cast<Language::Enum>(i)) << ": ";

				if (v > 0)
					ofs << "Yes - ";
				else
					ofs << "No - ";
				ofs << to_string(abs(v) * 100) << "%" << endl;

				++i;
			}	

			ofs  << endl;
		}

		ofs.close();
	}
}