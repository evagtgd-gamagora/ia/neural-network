#pragma once
#include <string>
#include <vector>
#include <map>

extern const std::string f_data = "../files/data/";
extern const std::string f_sources = "../files/sources/";
extern const std::string f_tests = "../files/tests/";

extern const std::string n_sources = "frequency-sources.data";
extern const std::string n_tests = "frequency-tests.data";
extern const std::string n_results = "test-results.data";
extern const std::string n_net = "language-net.net";


extern const int nLetter = 'Z' - 'A' + 1;

struct Language {

	static const int N = 3;

	enum Enum {
		english,
		french,
		german,
	};

	static const std::map<Enum, const char *> map;

	static const char* string(Enum e)
	{
		auto	it = map.find(e);
		return	it == map.end() ? "Language not found" : it->second;
	}
};

const std::map<Language::Enum, const char *> Language::map = {
	{Language::english, "english"},
	{Language::french, "french"},
	{Language::german, "german"},
};

struct Letter {

	static const int N = 3;

	enum Enum {
		//q,
		h,
		t,
		//u,
		//a,
		s,
	};

	static const std::map<Enum, const char> map;

	static const char ch(Enum e)
	{
		auto	it = map.find(e);
		return	it == map.end() ? '\n' : it->second;
	}
};

const std::map<Letter::Enum, const char> Letter::map = {
	//{Letter::q, 'q'},
	{Letter::h, 'h'},
	{Letter::t, 't'},
	//{Letter::u, 'u'},
	//{Letter::a, 'a'},
	{Letter::s, 's'},
};

struct LearnData
{
	std::vector<double> inputs;
	std::vector<double> outputs;
};

struct TestData
{
	std::string id;
	std::vector<double> inputs;
};

struct ResultData
{
	TestData testData;
	std::vector<double> outputs;
};