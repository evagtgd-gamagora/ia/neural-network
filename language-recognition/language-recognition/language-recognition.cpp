// language-recognition.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <string>
#include "fann.h"
#include "common.h"

using namespace std;

int main()
{
	const string source = f_data + n_sources;
	const string net = f_data + n_net;

	//Inputs : frequency of letters q h t u f
	//Outputs: language english french german
	const unsigned int num_input = Letter::N;
	const unsigned int num_output = Language::N;
	const unsigned int num_layers = 3;
	//'the optimal size of the hidden layer is usually between the size of the input and size of the output layers'. Jeff Heaton
	const unsigned int num_neurons_hidden = Letter::N +1 ; // Calcul ? Xor - 3 / here 7 -> ??? input + output = 8
	const float desired_error = (const float) 0.001;
	const unsigned int max_epochs = 500000;
	const unsigned int epochs_between_reports = 1000;

	struct fann *ann = fann_create_standard(num_layers, num_input, num_neurons_hidden, num_output);

	fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);

	fann_train_on_file(ann, source.c_str(), max_epochs, epochs_between_reports, desired_error);

	fann_save(ann, net.c_str());

	fann_destroy(ann);

	return 0;
}