// language-recognition-test.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include "fann.h"

using namespace std;
int main()
{
	const unsigned int num_input = 2;
	const unsigned int num_output = 1;
	const unsigned int num_test = 4;
	
	struct fann *ann = fann_create_from_file("../xor_float.net");
	fann_type test_input[num_test][num_input] = {
		{-1.0f, -1.0f},
		{-1.0f, 1.0f},
		{1.0f, -1.0f},
		{1.0f, 1.0f},
	};

	fann_type * pInputs = NULL;
	fann_type * pOutputs = NULL;

	cout << "XOR : " << endl;
	for (int i = 0; i < num_test; ++i)
	{
		cout << "Inputs : " << test_input[i][0] << " xor " << test_input[i][1] << endl;
		pInputs = &test_input[i][0];
		pOutputs = fann_run(ann, pInputs);
		cout << "Ouput : " << *pOutputs << endl;
	}

	fann_destroy(ann);
	system("pause");

	return 0;
}